module Sudoku where


import Data.Char
import Data.List
import Data.Set (difference, fromList, toList)



data Val = Empty | V Int
    deriving (Eq, Ord)

data Sudoku = Sudoku [[Val]]
    deriving (Eq)

data NinePart = L [Val] | C [Val] | S [Val] | NinePartEmpty
    deriving (Eq)


instance Show Val where
    show Empty = "'"
    show (V x) = show x

instance Show NinePart where
    show (L (x:xs)) = "L" ++ show (x:xs)
    show (C (x:xs)) = "C" ++ show (x:xs)
    show (S (x:xs)) = "S" ++ show (x:xs)
    show (NinePartEmpty) = "///"

lineSudokuSeparation :: [Val] -> String
lineSudokuSeparation (a0:a1:a2:a3:a4:a5:a6:a7:a8:[])
    = "|" ++ show a0 ++ show a1 ++ show a2 ++
      "|" ++ show a3 ++ show a4 ++ show a5 ++
      "|" ++ show a6 ++ show a7 ++ show a8 ++ "|"

instance Show Sudoku where
    show (Sudoku []) = "Null case"
    show (Sudoku (a0:a1:a2:a3:a4:a5:a6:a7:a8:[])) =
        "*---*---*---*" ++ "\n" ++
        lineSudokuSeparation a0 ++ "\n" ++
        lineSudokuSeparation a1 ++ "\n" ++
        lineSudokuSeparation a2 ++ "\n" ++
        "*---*---*---*" ++ "\n" ++
        lineSudokuSeparation a3 ++ "\n" ++
        lineSudokuSeparation a4 ++ "\n" ++
        lineSudokuSeparation a5 ++ "\n" ++
        "*---*---*---*" ++ "\n" ++
        lineSudokuSeparation a6 ++ "\n" ++
        lineSudokuSeparation a7 ++ "\n" ++
        lineSudokuSeparation a8 ++ "\n" ++
        "*---*---*---*\n"
    show (Sudoku (x:xs)) = "???"


makeEmptySudoku :: Sudoku
makeEmptySudoku =
    Sudoku (replicate 9 (replicate 9 (Empty)))

-- End this one
addValueSudoku :: Sudoku -> Int -> Int -> Val -> Sudoku
addValueSudoku (Sudoku sudoku) r c v |
    (r >= 0) && (r <= 8) && (c >= 0) && (c <= 8) && ((sudoku !! r !! c) == Empty) = Sudoku new_sudoku
                               | otherwise = Sudoku []
    where
        new_sudoku = (take r sudoku) ++ [new_line] ++ (drop (r+1) sudoku)
        new_line = (take c (sudoku !! r)) ++ [v] ++ (drop (c+1) (sudoku !! r))

getValueSudoku :: Sudoku -> Int -> Int -> Val
getValueSudoku (Sudoku s) r c = (s !! r) !! c

--fillFromString utils

char2Val :: Char -> Val
char2Val x = case x of
    '0' -> Empty
    '1' -> V 1
    '2' -> V 2
    '3' -> V 3
    '4' -> V 4
    '5' -> V 5
    '6' -> V 6
    '7' -> V 7
    '8' -> V 8
    '9' -> V 9
    otherwise -> error "Unexpected case in char2Val"

cleanString :: String -> String
cleanString = filter isDigit

fillFromString :: String -> Sudoku
fillFromString str = foldr (\((x, y), v) s -> addValueSudoku s x y v) makeEmptySudoku fillList
    where fillList = zip ((\x y -> (x,y)) <$> [0..8] <*> [0..8])  (map char2Val (cleanString str))


-- Check sudoku subparts
getLineSudoku :: Sudoku -> Int -> [Val]
getLineSudoku (Sudoku s) n = s !! n

getColumnSudoku :: Sudoku -> Int -> [Val]
getColumnSudoku (Sudoku s) n = (transpose s) !! n

-- get three lines, transpose them, get three again, transpose again, merge into a list
getSquareSudoku :: Sudoku -> Int -> [Val]
getSquareSudoku (Sudoku [a0, a1, a2, a3, a4, a5, a6, a7, a8]) n
    | r == 0 = q [a0, a1, a2]
    | r == 1 = q [a3, a4, a5]
    | r == 2 = q [a6, a7, a8]
    where r = n `mod` 3
          c = n `div` 3
          q x = concat (transpose (take 3 (drop (3 * c) (transpose x))))

isValidPart :: [Val] -> Bool
isValidPart xs = not (thereAreMultiple (sort (filter (/= Empty) xs)))

thereAreMultiple :: Ord a => [a] -> Bool
thereAreMultiple []     = False
thereAreMultiple (x:[]) = False
thereAreMultiple (x:y:xs) = if (x == y)
                            then True
                            else thereAreMultiple (y:xs)


-- Thanks https://sandiway.arizona.edu/sudoku/examples.html for the testing cases

testSudoku1 :: Sudoku
testSudoku1 = fillFromString "000260701 680070090 190004500 820100040 004602900 050003028 009300074 040050036 703018000"

testSudoku2 :: Sudoku
testSudoku2 = fillFromString "100489006 730000040 000001295 007120600 500703008 006095700 914600000 020000037 800512004"

testSudoku3 :: Sudoku
testSudoku3 = fillFromString "020608000 580009700 000040000 370000500 600000004 008000013 000020000 009800036 000306090"

testSudoku4 :: Sudoku
testSudoku4 = fillFromString "000600400 700003600 000091080 000000000 050180003 000306045 040200060 903000000 020000100"

testSudoku5 :: Sudoku
testSudoku5 = fillFromString "200300000 804062003 013800200 000020390 507000621 032006000 020009140 601250809 000001002"

testSudoku6 :: Sudoku
testSudoku6 = fillFromString "020000000 000600003 074080000 000003002 080040010 600500000 000010780 500009000 000000040"



dedup :: (Eq a) => [a] -> [a]
dedup [] = []
dedup [x] = [x]
dedup (x:y:xs) | (x == y)  = dedup (y:xs)
               | otherwise = x : dedup (y:xs)

-- Returns [Val] that doesn't contain values in the original list
invertValList :: [Val] -> [Val]
-- Invalid case
invertValList [] = []
-- Valid case 
invertValList xs = toList (difference (fromList (map char2Val['0'..'9'])) (fromList xs))



-- Sudoku utility
applyMoves :: Sudoku -> [(Int, Int, [Val])] -> [Sudoku]
applyMoves s [] = []
applyMoves s ((_, _, []) : xs)     = applyMoves s xs
applyMoves s ((r, c, (y:ys)) : xs) = (addValueSudoku s r c y) : applyMoves s ((r, c, ys) : xs)


checkFull :: Sudoku -> Bool
checkFull (Sudoku s) = null (filter (== Empty) (concat s))


isValid :: Sudoku -> Bool
isValid (Sudoku s) = foldr (&&) True (map isValidPart s)


clearNoCase :: [Sudoku] -> [Sudoku]
clearNoCase = filter (/= (Sudoku []))


genericSolver :: ([Sudoku] -> [Sudoku]) -> [Sudoku] -> Sudoku
genericSolver f xs = if ((any checkFull xs) || (null xs))
                      then
                          if (null xs)
                          then makeEmptySudoku
                          else head (filter checkFull xs)
                      else
                          genericSolver f (f xs)



-- Bruteforce strategy (generate all moves, then expand the tree)
trivialtoOccupyVals :: Sudoku -> Int -> Int -> [Val]
trivialtoOccupyVals s r c | (getValueSudoku s r c) == Empty =
    (invertValList . dedup . sort . concat) [getLineSudoku s r, getColumnSudoku s c, getSquareSudoku s i]
                           | otherwise = []
    where i = r `div` 3 + 3 * (c `div` 3)

allTrivialMoves :: Sudoku -> [(Int, Int, [Val])]
allTrivialMoves s = filter (\(_,_,x) -> x /= [])
                    [(r, c, trivialtoOccupyVals s r c) | r <- [0..8], c <- [0..8]]


bruteforceNextMove :: [Sudoku] -> [Sudoku]
bruteforceNextMove [] = []
bruteforceNextMove xs = filter isValid ( concat (map ( \x -> applyMoves x (allTrivialMoves x) ) xs) )

bruteforceSolver :: Sudoku -> Sudoku
bruteforceSolver s = genericSolver bruteforceNextMove [s]


-- Least diverging tree strategy (generate all moves, select the position that has the shortest branching tree)

-- The solver searches for at least one solution of the problem,
-- if there is none it's guaranteed an empty result is returned

sortedMultiPathMoves :: Sudoku -> [(Int, Int, [Val])]
sortedMultiPathMoves s = sortOn (\(_,_,x) -> length x) (allTrivialMoves s)

safeHeadList :: [a] -> [a]
safeHeadList [] = []
safeHeadList (x:xs) = [x]

leastDivergingTreeMove :: [Sudoku] -> [Sudoku]
leastDivergingTreeMove [] = []
leastDivergingTreeMove xs =
    filter isValid ( concat (map ( \x -> applyMoves x (safeHeadList (sortedMultiPathMoves x)) ) xs) )

leastDivergingSolver :: Sudoku -> Sudoku
leastDivergingSolver s = genericSolver leastDivergingTreeMove [s]

