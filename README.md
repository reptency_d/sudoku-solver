# Sudoku Solver

A sudoku solver written in Haskell.

## Usage
Use GHCi and import the module.

- `fillFromString "<sudoku raw by raw, only numbers and spaces with 0 for empty places>"` to import the sudoku,
- `leastDivergingSolver <sudoku>` to use the solver.

## License
GNU General Public License, version 3.
